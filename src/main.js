// src/main.js

const express = require('express');
const app = express();
const port = 3000;
const mylib = require('./mylib');

app.get('/', (req,res) => {
    res.send('Hello world');
});

// http://localhost:3000/add?a=2&b=3
app.get('/add', (req,res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    if(isNaN(a) || isNaN(b)){
        res.send(`a:${a} b:${b}`);
        return;
    }
    res.send(''+mylib.sum(a,b));
});
app.get('/funcs', (req,res) => {
    res.send({
        sum: mylib.sum(4,5),
        random: mylib.random(100,120),
        arrayGen: mylib.arrayGen(1,2,3,"string")
    });
});

app.listen(port, () => {
    console.log('Server: http://localhost:' + port);
})