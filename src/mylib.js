// src/mylib.js

module.exports = {
    sum: (a, b) => a + b,
    random: (min, max) => min + (Math.round(Math.random() * (max - min))),
    arrayGen: function(){
        let arr = [];
        for(let arg of arguments){
            arr.push(arg);
        }
        return arr;
    }
}