const expect = require('chai').expect;
const should = require('chai').should();
const mylib = require('../src/mylib')

describe('Parametrized unit testing mylib.js', () => {

    let myVar = undefined;
    let myMinVal = undefined;
    let myMaxVal = undefined;

    before(() => {
        console.log('Before testing...');
        myVar = 2;
        myMinVal = 10;
        myMaxVal = 20;
    });

    it('Myvar should exist', () => {
        should.exist(myVar);
    });

    it('parametrized way of unit testing', () => {
        const result = mylib.sum(myVar, myVar);
        expect(result).to.equal(myVar + myVar);
    });

    it('parametrized way of random value testing', () => {
        const result = mylib.random(myMinVal, myMaxVal);
        expect(result).to.above(myMinVal - 1).below(myMaxVal + 1);
    });

    it('testing arraygenerator', () => {
        const arr = [1,3,["asd"],{sad:1}]
        const genArr = mylib.arrayGen(1, 3, ['asd'], {sad:1});
        let result = JSON.stringify(arr) === JSON.stringify(genArr);
        expect(result).to.equal(true);
    });
    
    after(() => {
        console.log('After testing...');
    });

})