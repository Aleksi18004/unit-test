// test/example.test.js

const expect = require('chai').expect;
const { assert } = require('chai');
const mylib = require('../src/mylib')

describe('Unit testing mylib.js', () => {

    it('Should return 2 using sum function a=1, b=1', () => {
        const result = mylib.sum(1, 1); // 1 + 1
        expect(result).to.equal(2); // result expected to equal 2
    });

    it('Assert foo is not bar', () => {
        assert('foo' !== 'bar') // true
    });

})